package com.example.interbanderasapp;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class ListActivity extends AppCompatActivity {

    ListView listView;
    String mTitle[] = {"Banderola Gota", "Banderola Ala/Vela", "Banderola Easy", "Banderola Big", "Test"};
    String mDescription[] = {"S,M,L,XL", "S,M,L,XL", "M,L", "XL", "Test description"};
    int images[] = {R.drawable.gota, R.drawable.placeholder_ala, R.drawable.interbanderaslogo, R.drawable.interbanderaslogo, R.drawable.interbanderaslogo};
    // so our images and other things are set in array

    // now paste some images in drawable

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        listView = findViewById(R.id.listView);
        // now create an adapter class

        MyAdapter adapter = new MyAdapter(this, mTitle, mDescription, images);
        listView.setAdapter(adapter);
        // there is my mistake...
        // now again check this..

        // now set item click on list view
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position ==  0) {
                    Toast.makeText(ListActivity.this, "Banderola Gota", Toast.LENGTH_SHORT).show();
                }
                if (position ==  1) {
                    Toast.makeText(ListActivity.this, "Banderola Ala/Vela", Toast.LENGTH_SHORT).show();
                }
                if (position ==  2) {
                    Toast.makeText(ListActivity.this, "Banderola Easy", Toast.LENGTH_SHORT).show();
                }
                if (position ==  3) {
                    Toast.makeText(ListActivity.this, "Banderola Big", Toast.LENGTH_SHORT).show();
                }
                if (position ==  4) {
                    Toast.makeText(ListActivity.this, "Test Description", Toast.LENGTH_SHORT).show();
                }
            }
        });
        // so item click is done now check list view
    }

    class MyAdapter extends ArrayAdapter<String> {

        Context context;
        String rTitle[];
        String rDescription[];
        int rImgs[];

        MyAdapter (Context c, String title[], String description[], int imgs[]) {
            super(c, R.layout.row, R.id.tv_main, title);
            this.context = c;
            this.rTitle = title;
            this.rDescription = description;
            this.rImgs = imgs;

        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = layoutInflater.inflate(R.layout.row, parent, false);
            ImageView images = row.findViewById(R.id.image);
            TextView myTitle = row.findViewById(R.id.tv_main);
            TextView myDescription = row.findViewById(R.id.tv_sub);

            // now set our resources on views
            images.setImageResource(rImgs[position]);
            myTitle.setText(rTitle[position]);
            myDescription.setText(rDescription[position]);




            return row;
        }
    }
}
